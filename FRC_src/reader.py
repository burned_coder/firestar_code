import pandas as pd
from error import CustomError

class Reader:
    '''Class that read different types of files'''

    def __init__(self, directorio):
        self._directorioFichero = directorio    

    def readXlsxInfo(self):
        '''Method to read Xlsx Files'''

        try:
            readInfo = pd.read_excel(self._directorioFichero)        
            return readInfo   
        except FileNotFoundError:
            raise CustomError("File Not Found")
        except ValueError:
            raise CustomError("Selected file isn't correct")     
        except:
            raise CustomError("A Error Occurred")      

    def readJsonInfo(self):
        '''Method to read Json Files'''

        try:
            readInfo = pd.read_json(self._directorioFichero)
            return readInfo
        except FileNotFoundError:
            raise CustomError("File Not Found")
        except ValueError:
            raise CustomError("Selected file isn't correct")
        except:
            raise CustomError("A Error Occurred")

    def readXmlInfo(self):
        '''Method to read Xml Files'''

        try:
            readInfo = pd.read_xml(self._directorioFichero)
            return readInfo
        except FileNotFoundError:
            raise CustomError("File Not Found")
        except ValueError:
            raise CustomError("Selected file isn't correct")
        except:
            raise CustomError("A Error Occurred")
       