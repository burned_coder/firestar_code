class CustomError(Exception):
    '''Custom Exception Class'''
    def __init__(self, message):
        super().__init__(message)