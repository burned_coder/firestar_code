from reader import Reader
from listFiles import ListFiles
from rich.console import Console
from writer import Writer
from error import CustomError

def main():
    
    salir = False
    console = Console()

    while salir == False: 

        console.print()
        console.print("[bold yellow]    ,---,.                           .--.--.       ___                                  ,----..                                                       ___                        [/bold yellow]")
        console.print("[bold yellow]  ,'  .' |  ,--,                    /  /    '.   ,--.'|_                               /   /   \                                                    ,--.'|_                      [/bold yellow]")
        console.print("[bold yellow],---.'   |,--.'|    __  ,-.        |  :  /`. /   |  | :,'              __  ,-.        |   :     :  ,---.        ,---,                      __  ,-.  |  | :,'             __  ,-. [/bold yellow]")
        console.print("[bold yellow]|   |   .'|  |,   ,' ,'/ /|        ;  |  |--`    :  : ' :            ,' ,'/ /|        .   |  ;. / '   ,'\   ,-+-. /  |     .---.         ,' ,'/ /|  :  : ' :           ,' ,'/ /| [/bold yellow]")
        console.print("[bold yellow]:   :  :  `--'_   '  | |' | ,---.  |  :  ;_    .;__,'  /    ,--.--.  '  | |' |        .   ; /--` /   /   | ,--.'|'   |   /.  ./|  ,---.  '  | |' |.;__,'  /     ,---.  '  | |' | [/bold yellow]")
        console.print("[bold yellow]:   |  |-,,' ,'|  |  |   ,'/     \  \  \    `. |  |   |    /       \ |  |   ,'        ;   | ;   .   ; ,. :|   |  ,'' | .-' . ' | /     \ |  |   ,'|  |   |     /     \ |  |   ,' [/bold yellow]")
        console.print("[bold yellow]|   :  ;/|'  | |  '  :  / /    /  |  `----.   \:__,'| :   .--.  .-. |'  :  /          |   : |   '   | |: :|   | /  | |/___/ \: |/    /  |'  :  /  :__,'| :    /    /  |'  :  /   [/bold yellow]")
        console.print("[bold yellow]|   |   .'|  | :  |  | ' .    ' / |  __ \  \  |  '  : |__  \__\/: . .|  | '           .   | '___'   | .; :|   | |  | |.   \  ' .    ' / ||  | '     '  : |__ .    ' / ||  | '    [/bold yellow]")
        console.print("[bold yellow]'   :  '  '  : |__;  : | '   ;   /| /  /`--'  /  |  | '.'| , .--.; |;  : |            '   ; : .'|   :    ||   | |  |/  \   \   '   ;   /|;  : |     |  | '.'|'   ;   /|;  : |    [/bold yellow]")
        console.print("[bold yellow]|   |  |  |  | '.'|  , ; '   |  / |'--'.     /   ;  :    ;/  /  ,.  ||  , ;           '   | '/  :\   \  / |   | |--'    \   \  '   |  / ||  , ;     ;  :    ;'   |  / ||  , ;    [/bold yellow]")
        console.print("[bold yellow]|   :  \  ;  :    ;---'  |   :    |  `--'---'    |  ,   /;  :   .'   \---'            |   :    /  `----'  |   |/         \   \ |   :    | ---'      |  ,   / |   :    | ---'     [/bold yellow]")
        console.print("[bold yellow]|   | ,'  |  ,   /        \   \  /                ---`-' |  ,     .-./                 \   \ .'           '---'           '---' \   \  /             ---`-'   \   \  /           [/bold yellow]")
        console.print("[bold yellow]`----'     ---`-'          `----'                         `--`---'                      `---`                                    `----'                        `----'            [/bold yellow]")
        print()
        print()
        print()
        
        console.print("Choose a option: ",  style="bold yellow")
        console.print("[bold yellow]0.[/bold yellow] Exit",  style="bold purple")
        console.print("[bold yellow]1.[/bold yellow] Xlsx to Json",  style="bold red")
        console.print("[bold yellow]2.[/bold yellow] Xlsx to Xml",  style="bold red")
        console.print("[bold yellow]3.[/bold yellow] Json to Xml",  style="bold blue")
        console.print("[bold yellow]4.[/bold yellow] Xml to Json",  style="bold blue")
        console.print("[bold yellow]5.[/bold yellow] Several Xlsx to Json",  style="bold green")
        console.print("[bold yellow]6.[/bold yellow] Several Xlsx to Xml",  style="bold green")
        console.print("[bold yellow]7.[/bold yellow] Several Json to Xml",  style="bold magenta")
        console.print("[bold yellow]8.[/bold yellow] Several Xml to Json",  style="bold magenta")
        option = console.input("[bold yellow]> [/bold yellow]")
        try:
            if option == "0":         
                salir = True
            elif option == "1": 
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")     
                xslRead = Reader(fich)
                xslArray = xslRead.readXlsxInfo()
                jsonW = Writer()
                jsonW.jsonWriter(xslArray,fich)
            elif option == "2":  
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")     
                xslRead = Reader(fich)
                xslArray = xslRead.readXlsxInfo()
                xmlW = Writer()
                xmlW.xmlWriter(xslArray, fich)
            elif option == "3":
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")
                jtx = Reader(fich)
                json = jtx.readJsonInfo()
                xmlW = Writer()
                xmlW.xmlWriter(json, fich)
            elif option == "4":
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")
                xtj = Reader(fich)
                xml = xtj.readXmlInfo()
                jsonW = Writer()
                jsonW.jsonWriter(xml,fich)
            elif option == "5": 
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")       
                lf = ListFiles(fich, '.xlsx', 'json')
                lf.createList()
                lf.XlsxReadFiles()
            elif option == "6": 
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")       
                lf = ListFiles(fich, '.xlsx', 'xml')
                lf.createList()
                lf.XlsxReadFiles()
            elif option == "7":
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")
                lf = ListFiles(fich, '.json', 'xml')
                lf.createList()
                lf.jsonToXmlFiles()
            elif option == "8":
                fich = console.input("[bold yellow]Enter Path: [/bold yellow]")
                lf = ListFiles(fich, '.xml', 'json')
                lf.createList()
                lf.xmlToJsonFiles()
            elif option == "creator":
                console.print()
            else:
                console.print("Numero Incorrecto",  style="bold red")

            print()
            console.print("------------------------------------",  style="bold yellow")
            print()
        except CustomError as ce:
            print(ce)
        

main()