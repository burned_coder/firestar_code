from cgitb import text
from optparse import Values
import PySimpleGUI as sg
import os

from matplotlib.pyplot import title
from reader import Reader
from listFiles import ListFiles
from writer import Writer
from error import CustomError

def ended():
    sg.popup_ok('Files have been converted. Congratulations',title='Files Converted')

sg.theme('DarkAmber')

workingDirectory = os.getcwd()

layout = [[sg.Text('Select the file or the path of the files: ')],            
          [sg.InputText(key='filePath'), sg.FileBrowse('  File Browse  ', initial_folder=workingDirectory, file_types=[("All Files", "*.*"), ("Csv Files", "*.csv"), ("Xlsx Files", "*.xlsx"), ("Xml Files", "*.xml"), ("Json Files", "*.json")])],
          [sg.InputText(key='dirPath'), sg.FolderBrowse('Folder Browse', initial_folder=workingDirectory)],
          [sg.Text('')],   
          [sg.Text('  Convert One File: ')],  
          [sg.Text('')],       
          [sg.Radio(text='Xlsx to Json', group_id='choose', key='xlsx2json'), sg.Radio(text='Xlsx to Xml', group_id='choose', key='xlsx2xml')],
          [sg.Text('')],
          [sg.Radio(text='Json to Xml', group_id='choose', key='json2xml'), sg.Radio(text='Xml to Json', group_id='choose', key='xml2json')],
          [sg.Text('')], 
          [sg.Text('  Convert Several Files: ')], 
          [sg.Text('')],
          [sg.Radio(text='Several Xlsx Files to Json', group_id='choose', key='sevxlsx2json'), sg.Radio(text='Several Xlsx Files to Xml', group_id='choose', key='sevxlsx2xml')],
          [sg.Text('')],
          [sg.Radio(text='Several Json Files to Xml', group_id='choose', key='sevjson2xml'), sg.Radio(text='Several Xml Files to Json', group_id='choose', key='sevxml2json')],
          [sg.Text('')],
          [sg.Text('                              '), sg.Button('   Start   ', key='startButton'), sg.Exit('   Exit   ', key='Exit')],
          [sg.HorizontalSeparator()],
          [sg.Text('Burned Coder', key='key2')]]


window = sg.Window("Firestar Converter", layout)

while True:
    event, values = window.read()
    try:
        if event == 'startButton':
            if (values['xlsx2json'] or values['xlsx2xml'] or values['json2xml'] or values['xml2json']) and values['filePath'] != '' and values['dirPath'] == '':
                if values['xlsx2json']:
                    fich = values['filePath']    
                    xslRead = Reader(fich)
                    xslArray = xslRead.readXlsxInfo()
                    jsonW = Writer()
                    jsonW.jsonWriter(xslArray,fich)
                    ended()
                elif values['xlsx2xml']:
                    fich = values['filePath']   
                    xslRead = Reader(fich)
                    xslArray = xslRead.readXlsxInfo()
                    xmlW = Writer()
                    xmlW.xmlWriter(xslArray, fich)
                    ended()
                elif values['json2xml']:
                    fich = values['filePath']
                    jtx = Reader(fich)
                    json = jtx.readJsonInfo()
                    xmlW = Writer()
                    xmlW.xmlWriter(json, fich)
                    ended()
                elif values['xml2json']:
                    fich = values['filePath']
                    xtj = Reader(fich)
                    xml = xtj.readXmlInfo()
                    jsonW = Writer()
                    jsonW.jsonWriter(xml,fich)
                    ended()
            elif (values['sevxlsx2json'] or values['sevxlsx2xml'] or values['sevjson2xml'] or values['sevxml2json']) and values['filePath'] == '' and values['dirPath'] != '':
                if values['sevxlsx2json']:
                    fich = values['dirPath']     
                    lf = ListFiles(fich, '.xlsx', 'json')
                    lf.createList()
                    lf.XlsxReadFiles()
                    ended()
                elif values['sevxlsx2xml']:
                    fich = values['dirPath']    
                    lf = ListFiles(fich, '.xlsx', 'xml')
                    lf.createList()
                    lf.XlsxReadFiles()
                    ended()
                elif values['sevjson2xml']:
                    fich = values['dirPath']
                    lf = ListFiles(fich, '.json', 'xml')
                    lf.createList()
                    lf.jsonToXmlFiles()
                    ended()
                elif values['sevxml2json']:
                    fich = values['dirPath']
                    lf = ListFiles(fich, '.xml', 'json')
                    lf.createList()
                    lf.xmlToJsonFiles()  
                    ended()      
            else:    
                sg.popup_error('Select the correct path before click on the start button. If you select for several files of a type, then click on Folder Browse, otherwise click on File Browse and search it', title='Error, follow the instructions')        
    except CustomError as ce:
        sg.popup_error(ce, title='Exception Occured')        
           
    if event == sg.WIN_CLOSED or event == 'Exit':
        break   

window.close()