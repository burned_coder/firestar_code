from error import CustomError
import pandas as pd

class Writer:
    '''Class that write Json or Xml Archives'''

    @staticmethod
    def jsonWriter(ar, fich): 
        '''Json Writer Method''' 

        try:
            json = ar.to_json()            

            '''Open archive or create to write in it'''
            with open(fich+".json", "a") as f:
                print(fich)
                f.write(json)                
        except IOError:
            raise CustomError("Writing Error")
        except:
            raise CustomError("A Error Occurred")

    @staticmethod
    def xmlWriter(ar, fich):   
        '''Xml Writer Method'''

        try:
            xml = ar.to_xml()            

            '''Open archive or create to write in it'''
            with open(fich+".xml", "a") as f:
                f.write(xml)
        except IOError:
            raise CustomError("Writing Error")
        except:
            raise CustomError("A Error Occurred")
