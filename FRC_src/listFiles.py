import os
import time

from reader import Reader
from writer import Writer
from error import CustomError

class ListFiles:
    '''Class created to translate several archives in one time'''

    def __init__(self, dir, typeOrigin, typeFinal):
        self._dir = dir
        self._typeOrigin = typeOrigin
        self._typeFinal = typeFinal
        self._array = list()
        
    def createList(self):
        '''Method that create a list with all the files of a type'''

        try:
            for file in os.listdir(self._dir):
                if file.endswith(self._typeOrigin):
                    self._array.append(file)
        except FileNotFoundError:
            raise CustomError("File Not Found")
        
    def XlsxReadFiles(self):
        '''Method for transform several Xlsx files to Json or Xml'''

        route = ""
        data = list()
        routes = list()

        for elem in self._array:            
            route = self._dir+"/"+elem
            routes.append(route)
            xl = Reader(route)
            data.append(xl.readXlsxInfo())
        
        option = ""
        gd = Writer()
        if self._typeFinal == "json":                   
            i = 0
            for elem in data:                
                gd.jsonWriter(elem, routes[i])
                i += 1
                '''Sleep every iteration for avoid strange results'''
                time.sleep(1)
        elif self._typeFinal == "xml":  
            print("xml")        
            i = 0
            for elem in data:
                gd.xmlWriter(elem, routes[i])
                i += 1
                '''Sleep every iteration for avoid strange results'''
                time.sleep(1)
     
    def xmlToJsonFiles(self):
        '''Method for transform several xml files to json'''

        route = ""
        data = list()
        routes = list()

        for elem in self._array:            
            route = self._dir+"/"+elem
            routes.append(route)
            xl = Reader(route)
            data.append(xl.readXmlInfo())
        
        option = ""
        gd = Writer()           
        i = 0
        for elem in data:
            gd.jsonWriter(elem, routes[i])
            i += 1
            '''Sleep every iteration for avoid strange results'''
            time.sleep(1)

    def jsonToXmlFiles(self):
        '''Method that transform several json files to xml'''

        route = ""
        data = list()
        routes = list()

        for elem in self._array:            
            route = self._dir+"/"+elem
            routes.append(route)
            xl = Reader(route)
            data.append(xl.readJsonInfo())
        
        option = ""
        gd = Writer()           
        i = 0
        for elem in data:
            gd.xmlWriter(elem, routes[i])
            i += 1
            '''Sleep every iteration for avoid strange results'''
            time.sleep(1)